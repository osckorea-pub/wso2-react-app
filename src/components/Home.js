import React from 'react';

const Home = () => {
    return (
        <main role="main" className="container">
            <div className="jumbotron">
                <h1>Welcome to PizzaShack!</h1>
                <p className="lead">We speak the good food language.</p>
            </div>
        </main>
    );
}

export default Home;
