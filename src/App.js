import React, { Component } from 'react';
import NavBar from './components/NavBar';
import Home from './components/Home';
import Menu from './components/Menu';
import Callback from './components/Callback';
import {BrowserRouter, Route} from 'react-router-dom';
import Auth from "./components/Auth";
const auth = new Auth();
class App extends Component {
    render() {
        return (
            <BrowserRouter>
                <NavBar auth={auth}/>

                <Route exact path="/" component={Home}/>
                <Route exact path="/callback" render={(props => <Callback {...props} auth={auth} />)}/>
                <Route exact path="/menu" render={(props => <Menu {...props} auth={auth} />)}/>
            </BrowserRouter>
        );
    }
}

export default App;
